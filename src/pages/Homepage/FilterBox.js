import React from "react";
import { Card, CardBody } from "reactstrap";

export default function FilterBox() {
  const handleChange = (e) => {};
  return (
    <Card
      style={{
        backgroundColor: "#1E242F",
      }}
    >
      <CardBody>
        <div>
          <h5 className="text-white-50">Categories</h5>
          <dl
            style={{
              color: "#fff",
              lineHeight: 2,
              fontWeight: "normal",
            }}
          >
            <dt>All</dt>
            <dt>iPhonee</dt>
            <dt>Samsung</dt>
            <dt>iPad</dt>
            <dt>MacBook</dt>
            <dt>Airpods</dt>
          </dl>
        </div>

        <div className="mt-5">
          <h5 className="text-white-50">Price Filter</h5>

          <div className="text-center">
            <input className="form-control" placeholder="Min" />
            <span className="text-white-50 p-5 mb-4">|</span>
            <input className="form-control" placeholder="Max" />
          </div>
        </div>

        <div className="mt-5">
          <h5 className="text-white-50">Storage</h5>
          <div>
            <label className="text-white-50">
              <input
                type="checkbox"
                checked={false}
                onChange={handleChange(this)}
              />
              <span className="ml-1">32GB</span>
            </label>
          </div>
          <div>
            <label className="text-white-50">
              <input
                type="checkbox"
                checked={false}
                onChange={handleChange(this)}
              />
              <span className="ml-1">64GB</span>
            </label>
          </div>
          <div>
            <label className="text-white-50">
              <input
                type="checkbox"
                checked={false}
                onChange={handleChange(this)}
              />
              <span className="ml-1">128GB</span>
            </label>
          </div>
          <div>
            <label className="text-white-50">
              <input
                type="checkbox"
                checked={false}
                onChange={handleChange(this)}
              />
              <span className="ml-1">512GB</span>
            </label>
          </div>
        </div>
      </CardBody>
    </Card>
  );
}
