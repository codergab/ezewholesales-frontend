import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
} from "reactstrap";

import ComputerImages from "../../assets/imac-images.png";
import Products from "./Products";
import FilterBox from "./FilterBox";

import { getProducts } from "../../services/ProductService";

class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      networkResponse: {},
      fetching: false,
    };
  }

  onSubmit = (e) => {
    console.log(e);
  };

  fetchProducts = async () => {
    const products = await getProducts();
    console.log(products, "s");
    this.setState({
      products: products.sellRequests.data,
      networkResponse: products,
    });
  };

  componentWillMount() {
    this.fetchProducts();
  }

  render() {
    return (
      <Container fluid={true}>
        <Row className="mb-5">
          <Col md={6} className="p-5">
            <div className="hero p-5">
              <h2 className="text-white text-uppercase">
                Shop our latest <br /> available stock here
              </h2>
              <br />
              <div className="hero-form">
                <Form inline>
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Input
                      style={{
                        width: "100%",
                      }}
                      type="email"
                      name="email"
                      id="exampleEmail"
                      placeholder="Enter Search Term (e.g iPhone x, 128GB or A1)"
                    />
                  </FormGroup>
                  <Button color="primary">
                    Search <i data-feather="arrow-right"></i>
                  </Button>
                </Form>
              </div>
            </div>
          </Col>
          <Col md={6}>
            <img src={ComputerImages} />
          </Col>
        </Row>
        <Row className="mt-5">
          <Col md={2}>
            <FilterBox />
          </Col>
          <Col md={1}></Col>
          <Col md={9}>
            <Row className="mb-5">
              <Col md={12}>
                <h4 className="text-white-50">Filter by Type</h4>
                <button className="btn btn-secondary text-white">
                  Sell Requests
                </button>
                <button className="btn btn-info text-white">
                  Buy Requests
                </button>
              </Col>
            </Row>
            {!this.state.fetching && (
              <Products products={this.state.products} />
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default HomePage;
