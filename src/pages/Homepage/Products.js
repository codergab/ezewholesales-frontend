import React from "react";
import { Card, CardImg, CardBody, Badge, Col, Row, Button } from "reactstrap";

import PhoneImage from "../../assets/iphone_11_pro_max.png";

export default function Products(props) {
  const products = props.products;
  return (
    <Row>
      {products.map((product, i) => (
        <Col
          key={i}
          md={3}
          style={{
            marginBottom: "4em",
          }}
        >
          <Card
            style={{
              backgroundColor: "#1E242F",
            }}
          >
            <CardBody>
              <div className="text-right">
                <Button size="sm" disabled color="secondary" outline>
                  {product.phone_condition}
                </Button>
              </div>
              <div className="text-left">
                <img src={PhoneImage} height={200}></img>
              </div>
              <div className="description mt-2">
                <h5 className="text-white-50">{product.phone_model}</h5>
                <h6 className="text-white-50">
                  {product.phone_status} | {product.phone_storage_capacity}
                </h6>
                <p className="text-white-50">Unit Price</p>
                <h5 className="text-white">${product.phone_price}</h5>
                <p className="text-white-50">1500 Available</p>
              </div>
              <div className="text-center mt-4">
                <Button color="primary" className="pr-4 pl-4">
                  Buy
                </Button>
              </div>
            </CardBody>
          </Card>
        </Col>
      ))}
    </Row>
  );
}
