export const FETCH_PRODUCTS = "/products";
export const FILTER_PRODUCTS = "/products/filter";
export const SEARCH_PRODUCTS = "/products/search";
