import axios from "axios";
import { BASE_URL } from "../utils/constants";

axios.defaults.baseURL = BASE_URL;
// axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN';
axios.defaults.headers.post["Content-Type"] = "application/json";

export default axios;
