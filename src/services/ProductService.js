import axios from "./axios";
import { FETCH_PRODUCTS, FILTER_PRODUCTS } from "../utils/api-routes";

export const getProducts = async () => {
  return await axios.get(FETCH_PRODUCTS).then((res) => res.data.data);
};
